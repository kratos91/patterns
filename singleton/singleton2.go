package singleton

import "sync"

var(
	once2 sync.Once
	instance2 *MyClass
)

type MyClass struct {
	attrib string
}

func (c *MyClass) SetAttr(val string) {
	c.attrib=val
}

func GetMyClass() *MyClass {
	once2.Do(func() {
		instance2=&MyClass{"first"}
	})
	return instance2
}

func (c *MyClass) GetAttr() string{
	return c.attrib
}

