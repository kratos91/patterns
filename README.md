## Compilar
```shell
$ > make all
```

## Ejemplos de patrones de diseño en Golang

### Patterns

* Builder
    - Parameter
    - Functional

* Factory
    - Function
    - Generator
    - Interface
    - Prototype
    - Abstract

* Prototype
    - Deep Copying

* Singleton

* Adapter

* Bridge

* Composite

* Decorator

* Command
