package builder

type PersonB struct {
	name, position string
}

type personMod func(*PersonB)

type PersonBuil struct {
	actions []personMod
}

func (b *PersonBuil) Called(name string) *PersonBuil {
	b.actions = append(b.actions, func(pb *PersonB) {
		pb.name = name
	})
	return b
}

func (b *PersonBuil) WorkAsA(position string) *PersonBuil {
	b.actions = append(b.actions, func(pb *PersonB) {
		pb.position = position
	})
	return b
}

func (b *PersonBuil) Build() *PersonB {
	p := PersonB{}
	for _, a := range b.actions {
		a(&p)
	}
	return &p
}
