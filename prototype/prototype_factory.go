package prototype

import (
	"bytes"
	"encoding/gob"
)

var (
	mainOffice = Empleado{"", Direccion{1, "123 East Dr", "London"}}
	auxOffice  = Empleado{"", Direccion{2, "321 East Dr", "London"}}
)

type Direccion struct {
	Suite               int
	StreetAddress, City string
}

func newEmpleado(proto *Empleado, name string, suite int) *Empleado {
	result := proto.DeepCopy()
	result.Name = name
	result.Office.Suite = suite
	return result
}

func NewMainOfficeEmpleado(name string, suite int) *Empleado {
	return newEmpleado(&mainOffice, name, suite)
}

func NewAuxOfficeEmpleado(name string, suite int) *Empleado {
	return newEmpleado(&auxOffice, name, suite)
}

type Empleado struct {
	Name   string
	Office Direccion
}

func (em *Empleado) DeepCopy() *Empleado {
	b := bytes.Buffer{}
	e := gob.NewEncoder(&b)
	_ = e.Encode(em)

	//fmt.Println(string(b.Bytes()))
	d := gob.NewDecoder(&b)
	result := Empleado{}
	_ = d.Decode(&result)

	return &result
}
