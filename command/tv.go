package command

import "fmt"

type Tv struct {
	IsRunning bool
}

func (t *Tv) On() {
	t.IsRunning=true
	fmt.Println("Turning Tv on")
}

func (t *Tv) Off() {
	t.IsRunning=false
	fmt.Println("Turning Tv off")
}
