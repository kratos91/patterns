package command

type Command interface {
	Execute()
}

type Button struct {
	Command Command
}

func (b *Button) Press() {
	b.Command.Execute()
}


