package main

import (
	"fmt"

	"bitbucket.org/kratos91/patterns/bridge"
	"bitbucket.org/kratos91/patterns/composite"
)

func main() {
	//Builder
	/*pb := builder.NewPersonBuilder()
	pb.
		Lives().At("Pozo Grande").In("Tecamac").WithPostcode("55745").
		Works().At("MercadoLibre").AsA("Programmer").Earning(123)

	person := pb.Build()
	fmt.Println(person)*/

	//Builder parameter
	/*	builder.SendEmail(func(b *builder.EmailBuilder) {
			b.From("foo@bar.com").
				To("bar@baz.com").
				Subject("Meeting").
				Body("Hello, do you want to meet?")
		})
	*/
	//Builder functional
	/*b := builder.PersonBuil{}
	p := b.Called("Dimitri").WorkAsA("Programmer").Build()
	fmt.Println(*p)
	*/

	//Factory
	/*p := factory.NewPerson("Jhon", 33)
	fmt.Println(p)

	p2 := factory.NewPersonBuild("Emiliano", 30)
	fmt.Println(p2)

	developerFactory := factory.NewEmployeeFactory("developer", 60000)
	developer := developerFactory("Adam")
	fmt.Println(developer)

	bossFactory := factory.NewEmployeeFactory2("CEO", 200000)
	boss := bossFactory.Create("Sam")
	fmt.Println(boss)

	m := factory.NewEmpleado(factory.Manager)
	m.Nombre = "Emiliano"
	fmt.Println(m)*/

	//Prototype

	/*jhon := prototype.Person{"John", &prototype.Address{"123 st", "London", "UK"}, []string{"Cris", "Matt"}}
	jane := jhon.DeepCopy()
	jane.Name = "Jane"
	jane.Address.Street = "321 Baker St"
	jane.Friends = append(jane.Friends, "Emiliano")

	fmt.Println(jhon, jhon.Address)
	fmt.Println(jane, jane.Address)

	pepe := prototype.NewMainOfficeEmpleado("pepe", 100)
	juana := prototype.NewAuxOfficeEmpleado("juana", 200)

	fmt.Println(pepe)
	fmt.Println(juana)*/

	//Singleton
	/*db := singleton.GetSingletonDatabase()
	pop := db.GetPopulation("Tokyo")
	fmt.Println(pop)*/

	//Adapter
	/*rc := adapter.NewRectangle(6, 4)
	a := adapter.VectorToRaster(rc)
	fmt.Println(adapter.DrawPoints(a))*/

	//Bridge

	raster := bridge.RasterRenderer{}
	vector := bridge.VectorRenderer{}

	circle := bridge.NewCircle(&raster, 5)
	circle.Draw()

	circle2 := bridge.NewCircle(&vector, 6)
	circle2.Draw()

	//Composite
	drawing := composite.GraphicObject{"My drawing", "", nil}
	drawing.Children = append(drawing.Children, *composite.NewCircle("Blue"))
	drawing.Children = append(drawing.Children, *composite.NewSquare("Red"))

	group := composite.GraphicObject{"Group 1", "", nil}
	group.Children = append(group.Children, *composite.NewCircle("Blue"))
	group.Children = append(group.Children, *composite.NewSquare("Blue"))
	drawing.Children = append(drawing.Children, group)

	fmt.Println(drawing.String())

	//Composite
	composite.ImplementationComposite()
	//Decorator
	/*circle := decorator.Circle{2}
	fmt.Println(circle.Render())

	redCircle := decorator.ColoredShape{&circle, "Red"}
	fmt.Println(redCircle.Render())

	rhsCircle := decorator.TransparentShape{&redCircle, 0.5}
	fmt.Println(rhsCircle.Render())*/
}
