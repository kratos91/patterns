package factory

type Empleado struct {
	Nombre, Posicion string
	SalarioAnual     int
}

const (
	Developer = iota
	Manager
)

func NewEmpleado(role int) *Empleado {
	switch role {
	case Developer:
		return &Empleado{"", "developer", 60000}
	case Manager:
		return &Empleado{"", "manager", 100000}
	default:
		panic("unsupported rola")
	}
}
