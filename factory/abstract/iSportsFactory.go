package abstract

import "fmt"

type iSportsFactory interface {
	MakeShoe() IShoe
	MakeShirt() IShirt
}

func NewSportsFactory(brand string) (iSportsFactory,error) {
	if brand == "adidas" {
		return &Adidas{}, nil
	}

	if brand == "nike" {
		return &Nike{}, nil
	}

	return nil, fmt.Errorf("Wrong brand type passed")
}
