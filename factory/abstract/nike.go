package abstract

type Nike struct {
}

func (n *Nike) MakeShoe() IShoe {
	return &NikeShoe{
		shoe: shoe{
			logo: "nike",
			size: 14,
		},
	}
}

func (n *Nike) MakeShirt() IShirt {
	return &NikeShirt{
		shirt: shirt{
			logo: "nike",
			size: 14,
		},
	}
}
