package main

import (
	"bitbucket.org/kratos91/patterns/factory/abstract"
	"fmt"
)

func main()  {
	adidasFactory,_:=abstract.NewSportsFactory("adidas")
	nikeFactory,_:=abstract.NewSportsFactory("nike")

	nikeShoe:=nikeFactory.MakeShoe()
	nikeShirt:=nikeFactory.MakeShirt()

	adidasShoe:=adidasFactory.MakeShoe()
	adidasShirt:=adidasFactory.MakeShirt()

	printShoeDetails(nikeShoe)
	printShirtDetails(nikeShirt)

	printShoeDetails(adidasShoe)
	printShirtDetails(adidasShirt)
}

func printShoeDetails(s abstract.IShoe) {
	fmt.Printf("Logo: %s", s.GetLogo())
	fmt.Println()
	fmt.Printf("Size: %d", s.GetSize())
	fmt.Println()
}

func printShirtDetails(s abstract.IShirt) {
	fmt.Printf("Logo: %s", s.GetLogo())
	fmt.Println()
	fmt.Printf("Size: %d", s.GetSize())
	fmt.Println()
}
