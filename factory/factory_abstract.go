package factory

type Reservation interface {}
type Invoice interface {}

type AbstractFactory interface {
	CreateReservation() Reservation
	CreateInvoice() Invoice
}

type HotelFactory struct {}

func (h *HotelFactory) CreateReservation() Reservation {
	return new(HotelReservation)
}

func (h *HotelFactory) CreateInvoice() Invoice {
	return new(HotelInvoice)
}

type FlightFactory struct {}

func (h *FlightFactory) CreateReservation() Reservation {
	return new(FlightReservation)
}

func (h *FlightFactory) CreateInvoice() Invoice {
	return new(FlightInvoice)
}

type HotelReservation struct {}
type HotelInvoice struct {}
type FlightReservation struct {}
type FlightInvoice struct {}

func GetFactory(vertical string) AbstractFactory{
	var factory AbstractFactory
	switch vertical {
	case "flight":
		factory=&FlightFactory{}
	case "hotel":
		factory=&HotelFactory{}
	}
	return factory
}