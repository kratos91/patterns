package factory

import "fmt"

type PersonInt interface {
	SayHello()
}

type person struct {
	name string
	age  int
}

type tiredPerson struct {
	name string
	age  int
}

func (p *person) SayHello() {
	fmt.Printf("Hi,my name is %s,I am %d years old \n", p.name, p.age)
}

func (tp *tiredPerson) SayHello() {
	fmt.Printf("I am so tired \n")
}

func NewPersonBuild(name string, age int) PersonInt {
	if age > 100 {
		return &tiredPerson{name, age}
	}
	return &person{name, age}
}
