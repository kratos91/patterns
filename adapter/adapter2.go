package adapter

import "fmt"

type Adaptee struct {}

func (a *Adaptee) ExistingMethod() {
	fmt.Println("using existing method")
}

type Adapter struct {
	adaptee *Adaptee
}

func NewAdapter() *Adapter {
	return &Adapter{adaptee: new(Adaptee)}
}

func (ad *Adapter) ExpectedMethod() {
	fmt.Println("doing something")
	ad.adaptee.ExistingMethod()
}